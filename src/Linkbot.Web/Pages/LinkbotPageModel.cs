﻿using Linkbot.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Linkbot.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class LinkbotPageModel : AbpPageModel
    {
        protected LinkbotPageModel()
        {
            LocalizationResourceType = typeof(LinkbotResource);
        }
    }
}