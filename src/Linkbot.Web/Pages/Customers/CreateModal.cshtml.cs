using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Linkbot.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Volo.Abp.AspNetCore.Mvc.UI.Alerts;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Volo.Abp.Data;
using Volo.Abp.ObjectExtending;

namespace Linkbot.Web.Pages.Customers
{
    public class CreateModalModel : LinkbotPageModel
    {
        [BindProperty]
        public CustomerDetailsModel CustomerDetails { get; set; }

        public List<SelectListItem> CustomerStatusList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "NEW LEAD", Text = "New Lead"},
            new SelectListItem { Value = "CUSTOMER", Text = "Customer"},
            new SelectListItem { Value = "VIP", Text = "VIP"},
            new SelectListItem { Value = "VVIP", Text = "VVIP"},
            new SelectListItem { Value = "INACTIVE", Text = "Inactive"}
        };

        public List<SelectListItem> SubscriptionList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "NEW", Text = "New"},
            new SelectListItem { Value = "SUBSCRIBED", Text = "Subscribed"},
            new SelectListItem { Value = "UNSUBSCRIBED", Text = "Unsubscribed"}
        };

        public bool IsEmailMandatory { get; set; }
        public bool IsMobileNoMandatory { get; set; }

        private readonly ICustomerDetailsAppService _customerDetailsAppService;
        private readonly ICustomerSettingsAppService _customerSettingsAppService;
        private readonly ICustomerCustomPropertyAppService _customerCustomPropertyAppService;

        public CreateModalModel(ICustomerDetailsAppService customerDetailsAppService
            , ICustomerSettingsAppService customerSettingsAppService
            , ICustomerCustomPropertyAppService customerCustomPropertyAppService)
        {
            _customerDetailsAppService = customerDetailsAppService;
            _customerSettingsAppService = customerSettingsAppService;
            _customerCustomPropertyAppService = customerCustomPropertyAppService;
        }

        public void OnGet()
        {
            CustomerDetails = new CustomerDetailsModel();
            var customPropertyList = _customerCustomPropertyAppService.GetCustomerCustomPropertyAsync().Result;
            var customerSettingsDto = _customerSettingsAppService.GetCustomerSettingsAsync().Result;

            if (customerSettingsDto.IsEmailMandatory)
                IsEmailMandatory = true;

            if(customerSettingsDto.IsMobileNumberMandatory)
                IsMobileNoMandatory = true;

            CustomerDetails.CustomExtraPropertySetting = customPropertyList;
            foreach (var customProperty in customPropertyList)
            {
                if(customProperty.CustomPropertyType == "string")
                    CustomerDetails.ExtraProperties.Add(customProperty.CustomPropertyName, "");
                else if (customProperty.CustomPropertyType == "int")
                    CustomerDetails.ExtraProperties.Add(customProperty.CustomPropertyName, 0);
                else if (customProperty.CustomPropertyType == "bool")
                    CustomerDetails.ExtraProperties.Add(customProperty.CustomPropertyName, false);
                else if (customProperty.CustomPropertyType == "datetime")
                    CustomerDetails.ExtraProperties.Add(customProperty.CustomPropertyName, DateTime.Today);
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var createUpdateCustomerDetailsDto = ObjectMapper.Map<CustomerDetailsModel, CreateUpdateCustomerDetailsDto>(CustomerDetails);

            //var customerSettingsDto = await _customerSettingsAppService.GetCustomerSettingsAsync();
            //createUpdateCustomerDetailsDto.IsEmailMandatory = customerSettingsDto.IsEmailMandatory;
            //createUpdateCustomerDetailsDto.IsMobileNumberMandatory = customerSettingsDto.IsMobileNumberMandatory;

            await _customerDetailsAppService.CreateAsync(createUpdateCustomerDetailsDto);
            return NoContent();
        }

        [AutoMap(typeof(CreateUpdateCustomerDetailsDto), ReverseMap = true)]
        public class CustomerDetailsModel: ExtensibleObject
        {
            [StringLength(128)]
            public string FirstName { get; set; }

            [StringLength(128)]
            public string LastName { get; set; }

            [RegularExpression(@"\d{8}$", ErrorMessage = "Invalid contact number.")]
            public string PhoneNumber { get; set; }

            [RegularExpression(@"\d{8}$", ErrorMessage = "Invalid contact number.")]
            public string MobileNumber { get; set; }

            [EmailAddress]
            public string EmailAddress { get; set; }

            public string AddressLine1 { get; set; }

            public string AddressLine2 { get; set; }

            [Range(0, Int64.MaxValue, ErrorMessage = "Invalid postal code.")]
            [StringLength(6)]
            public string PostalCode { get; set; }

            public string CompanyName { get; set; }

            [SelectItems(nameof(CustomerStatusList))]
            public string CustomerStatus { get; set; }


            [SelectItems(nameof(SubscriptionList))]
            public string Subscription { get; set; }

            [TextArea(Rows = 4)]
            public string Remark { get; set; }

            public List<CreateUpdateCustomPropertyDetailsDto> CustomExtraPropertySetting { get; set; }

        }
    }
    
}
