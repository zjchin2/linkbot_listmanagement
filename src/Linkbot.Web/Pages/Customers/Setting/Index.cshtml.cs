using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Linkbot.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Linkbot.Web.Pages.Customers.Setting
{
    public class IndexModel : LinkbotPageModel
    {
        [BindProperty]
        public CustomerSettingsModel CustomerSettings { get; set; }

        private readonly ICustomerSettingsAppService _customerSettingsAppService;
        private readonly ICustomerCustomPropertyAppService _customerCustomPropertyAppService;

        public IndexModel(ICustomerSettingsAppService customerSettingsAppService
            , ICustomerCustomPropertyAppService customerCustomPropertyAppService)
        {
            _customerSettingsAppService = customerSettingsAppService;
            _customerCustomPropertyAppService = customerCustomPropertyAppService;
        }

        public async Task OnGetAsync()
        {
            var customerSettingsDto = await _customerSettingsAppService.GetCustomerSettingsAsync();
            CustomerSettings = ObjectMapper.Map<CustomerSettingsDto, CustomerSettingsModel>(customerSettingsDto);
        }

        [AutoMap(typeof(CustomerSettingsDto), ReverseMap = true)]
        public class CustomerSettingsModel
        {
            [DisabledInput]
            [Display(Name = "Is Email Address mandatory?")]
            public bool IsEmailMandatory { get; set; } = true;

            [DisabledInput]
            [Display(Name = "Is Mobile Number mandatory?")]
            public bool IsMobileNumberMandatory { get; set; } = false;
        }
    }
}
