using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using Linkbot.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Linkbot.Web.Pages.Customers.Setting
{
    public class CreateCustomPropertyModalModel : LinkbotPageModel
    {
        [BindProperty]
        public CustomerCustomPropertyModel CustomPropertyDetails { get; set; }

        public List<SelectListItem> PropertyTypeList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "string", Text = "string"},
            new SelectListItem { Value = "int", Text = "int"},
            new SelectListItem { Value = "bool", Text = "bool"},
            new SelectListItem { Value = "datetime", Text = "datetime"}
        };

        private readonly ICustomerCustomPropertyAppService _customerCustomPropertyAppService;

        public CreateCustomPropertyModalModel(ICustomerCustomPropertyAppService customerCustomPropertysAppService)
        {
            _customerCustomPropertyAppService = customerCustomPropertysAppService;
        }

        public void OnGet()
        {
            CustomPropertyDetails = new CustomerCustomPropertyModel();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var customerCustomPropertyDto = ObjectMapper.Map<CustomerCustomPropertyModel, CustomerCustomPropertyDto>(CustomPropertyDetails);
            await _customerCustomPropertyAppService.CreateAsync(customerCustomPropertyDto);
            return NoContent();
        }

        [AutoMap(typeof(CustomerCustomPropertyDto), ReverseMap = true)]
        public class CustomerCustomPropertyModel
        {
            [Required]
            [StringLength(128)]
            [Display(Name = "Custom Property Name")]
            public string CustomPropertyName { get; set; }

            [Required]
            [SelectItems(nameof(PropertyTypeList))]
            [Display(Name = "Custom Property Type")]
            public string CustomPropertyType { get; set; }

            [Display(Name = "Is Host Property")]
            public bool IsHostProperty { get; set; }

        }
    }
}
