﻿$(function () {
    var l = abp.localization.getResource('Linkbot');

    //#region Mandatory Setting
    var editModal = new abp.ModalManager(abp.appPath + 'Customers/Setting/EditModal');

    var mandatoryEmail = document.getElementById("CustomerSettings_IsEmailMandatory");
    var mandatoryMobileNumber = document.getElementById("CustomerSettings_IsMobileNumberMandatory");

    editModal.onResult(function (e) {
        mandatoryEmail.checked = document.getElementById("CustomerSettings_IsEmailMandatory").checked;
        mandatoryMobileNumber.checked = document.getElementById("CustomerSettings_IsMobileNumberMandatory").checked;
    });

    $('#EditSettingButton').click(function (e) {
        e.preventDefault();
        editModal.open();
    });
    //#endregion

    //#region Custom Property
    var _customerCustomPropertyAppService = linkbot.customers.customerCustomProperty;

    var createCustomPropertyModal = new abp.ModalManager(abp.appPath + 'Customers/Setting/CreateCustomPropertyModal');
    var editCustomPropertyModal = new abp.ModalManager(abp.appPath + 'Customers/Setting/EditCustomPropertyModal');

    $(function () {
        var _$wrapper = $('#CustomPropertyWrapper');
        var _$table = _$wrapper.find('table');

        var dataTableCustomProperty = _$table.DataTable(
            abp.libs.datatables.normalizeConfiguration({
                order: [[1, 'asc']],
                processing: true,
                serverSide: true,
                scrollX: true,
                paging: true,
                searching: false,
                ajax: abp.libs.datatables.createAjax(_customerCustomPropertyAppService.getListPage),
                columnDefs: [
                    {
                        title: l('Actions'),
                        rowAction: {
                            items:
                                [
                                    {
                                        text: l('Edit'),
                                        visible: abp.auth.isGranted('Linkbot.CustomerSettings.CustomPropertyEdit'),
                                        action: function (data) {
                                            editCustomPropertyModal.open({ id: data.record.id });
                                        }
                                    },
                                    {
                                        text: l('Delete'),
                                        visible: abp.auth.isGranted('Linkbot.CustomerSettings.CustomPropertyDelete'),
                                        confirmMessage: function (data) {
                                            return l('CustomPropertyDeletionConfirmationMessage', data.record.customPropertyName);
                                        },
                                        action: function (data) {
                                            _customerCustomPropertyAppService
                                                .delete(data.record.id)
                                                .then(function () {
                                                    abp.notify.info(l('SuccessfullyDeleted'));
                                                    dataTableCustomProperty.ajax.reload();
                                                });
                                        }
                                    }
                                ]
                        }
                    },
                    {
                        title: l('CustomPropertyName'),
                        data: "customPropertyName"
                    },
                    {
                        title: l('CustomPropertyType'),
                        data: "customPropertyType"
                    }
                ]
            })
        );

        createCustomPropertyModal.onResult(function () {
            dataTableCustomProperty.ajax.reload();
        });

        editCustomPropertyModal.onResult(function () {
            dataTableCustomProperty.ajax.reload();
        });

        $('#NewCustomPropteryButton').click(function (e) {
            e.preventDefault();
            createCustomPropertyModal.open();
        });

        $('#divCustomerSetting a[href="#customer-setting-tabs_1"]').click(function () {
            setTimeout(function () {
                dataTableCustomProperty.columns.adjust();
            }, 200);
        });
    });
    //#endregion
});