using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Linkbot.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Linkbot.Web.Pages.Customers.Setting
{
    public class EditModalModel : LinkbotPageModel
    {
        [BindProperty]
        public CreateUpdateCustomerSettingsDto CustomerSettings { get; set; }

        private readonly ICustomerSettingsAppService _customerSettingsAppService;

        public EditModalModel(ICustomerSettingsAppService customerSettingsAppService)
        {
            _customerSettingsAppService = customerSettingsAppService;
        }

        public async Task OnGetAsync()
        {
            var customerSettingsDto = await _customerSettingsAppService.GetCustomerSettingsAsync();
            CustomerSettings = ObjectMapper.Map<CustomerSettingsDto, CreateUpdateCustomerSettingsDto>(customerSettingsDto);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _customerSettingsAppService.UpdateCustomerSettingsAsync(CustomerSettings);
            return NoContent();
        }
    }
}
