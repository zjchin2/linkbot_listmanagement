﻿(function ($) {

    var l = abp.localization.getResource('Linkbot');

    var _customerDetailsAppService = linkbot.customers.customerDetails;
    var _appointmentHistoryAppService = linkbot.appointment.appointmentHistory;
    var _transactionHistoryAppService = linkbot.transaction.transactionHistory;

    var createModal = new abp.ModalManager(abp.appPath + 'Customers/CreateModal');
    var editModal = new abp.ModalManager(abp.appPath + 'Customers/EditModal');

    $(function () {
        $('input[type="file"]').change(function (e) {
            var fileName = e.target.files[0].name;
            $('.custom-file-label').html(fileName);
        });

        var _$wrapper = $('#CustomerDetailsWrapper');
        var _$table = _$wrapper.find('#CustomersTable');
        var dataTableUpcomingAppt;

        var getFilter = function () {
            var filterObj = $('#CustomersFilterForm').serializeFormToObject();
            return filterObj;
        };

        var dataTable = _$table.DataTable(
            abp.libs.datatables.normalizeConfiguration({
                order: [[1, 'asc']],
                processing: true,
                serverSide: true,
                scrollX: true,
                paging: true,
                searching: false,
                ajax: abp.libs.datatables.createAjax(_customerDetailsAppService.getList, getFilter),
                columnDefs: columnDefVariable()
            })
        );

        function columnDefVariable() {
            var columns = [
                {
                    title: l('Actions'),
                    rowAction: {
                        items:
                            [
                                {
                                    text: l('Edit'),
                                    visible: abp.auth.isGranted('Linkbot.Customers.Edit'),
                                    action: function (data) {
                                        editModal.open({ id: data.record.id });
                                    }
                                },
                                {
                                    text: l('Appointment'),
                                    visible: abp.auth.isGranted('Linkbot.Appointment'),
                                    action: function (data) {
                                        upcomingApptModalOpen(data.record.id);
                                    }
                                },
                                {
                                    text: l('Purchases'),
                                    visible: abp.auth.isGranted('Linkbot.Transaction'),
                                    action: function (data) {
                                        purchaseHistoryModalOpen(data.record.id);
                                    }
                                },
                                {
                                    text: l('Delete'),
                                    visible: abp.auth.isGranted('Linkbot.Customers.Delete'),
                                    confirmMessage: function (data) {
                                        return l('CustomerDeletionConfirmationMessage', data.record.firstName);
                                    },
                                    action: function (data) {
                                        linkbot.customers.customerDetails
                                            .delete(data.record.id)
                                            .then(function () {
                                                abp.notify.info(l('SuccessfullyDeleted'));
                                                dataTable.ajax.reload();
                                            });
                                    }
                                }
                            ]
                    }
                },
                {
                    title: l('FirstName'),
                    data: "firstName"
                },
                {
                    title: l('LastName'),
                    data: "lastName"
                },
                {
                    title: l('MobileNumber'),
                    data: "mobileNumber"
                }
            ];

            return columns;
        }

        $('#CustomersFilterForm').submit(function (e) {
            e.preventDefault();
            dataTable.ajax.reload();
        });

        $('#Filter').bind('keyup', function () {
            $('#FilterHidden').val(this.value);
        });

        createModal.onResult(function () {
            dataTable.ajax.reload();
        });

        editModal.onResult(function () {
            dataTable.ajax.reload();
        });

        $('#NewCustomerButton').click(function (e) {
            e.preventDefault();
            createModal.open();
        });

        function upcomingApptModalOpen(custId) {
            var _$modal = $('#UpcomingApptModal');
            var _$table = _$modal.find('table');

            if ($.fn.DataTable.isDataTable('#UpcomingApptTable')) {
                _$table.DataTable().clear();
                _$table.DataTable().destroy();
            }

            //TODO: Fix calling twice issue
            dataTableUpcomingAppt = _$table.DataTable(
                abp.libs.datatables.normalizeConfiguration({
                    order: [[1, 'asc']],
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    paging: true,
                    searching: false,
                    ajax: abp.libs.datatables.createAjax(_appointmentHistoryAppService.getUpcomingApptListPage, function () {
                        return { filter: custId };
                    }),
                    columnDefs: [
                        {
                            title: l('AppointmentName'),
                            data: "appointmentName",
                        },
                        {
                            title: l('AppointmentDate'),
                            data: "appointmentDate",
                            dataFormat: 'datetime'
                        },
                        {
                            title: l('BookingId'),
                            data: "appointmentId"
                        },
                        {
                            title: l('AppointmentStatus'),
                            data: "appointmentStatus"
                        }
                    ]
                })
            );

            $('#UpcomingApptModal').modal('show');
            setTimeout(function () {
                dataTableUpcomingAppt.columns.adjust();
            }, 200);
        }

        function purchaseHistoryModalOpen(custId) {
            var _$modal = $('#PurchaseHistoryModal');
            var _$table = _$modal.find('table');

            if ($.fn.DataTable.isDataTable('#PurchaseHistoryTable')) {
                _$table.DataTable().clear();
                _$table.DataTable().destroy();
            }

            //TODO: Fix calling twice issue
            dataTablePurchaseHistory = _$table.DataTable(
                abp.libs.datatables.normalizeConfiguration({
                    order: [[1, 'asc']],
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    paging: true,
                    searching: false,
                    ajax: abp.libs.datatables.createAjax(_transactionHistoryAppService.getPurchaseHistory, function () {
                        return { filter: custId };
                    }),
                    columnDefs: [
                        {
                            title: l('TransactionReference'),
                            data: "transactionNo",
                        },
                        {
                            title: l('PurchaseItem'),
                            data: "purchaseItem",
                        },
                        {
                            title: l('TransactionDate'),
                            data: "transactionDate",
                            dataFormat: 'date'
                        },
                        {
                            title: l('PurchaseQuantity'),
                            data: "quantity",
                        },
                        {
                            title: l('UnitPrice'),
                            data: "unitPrice"
                        },
                        {
                            title: l('Amount'),
                            data: "amount"
                        }
                    ]
                })
            );

            $('#PurchaseHistoryModal').modal('show');
            setTimeout(function () {
                dataTablePurchaseHistory.columns.adjust();
            }, 200);
        }
        
    });
})(jQuery);