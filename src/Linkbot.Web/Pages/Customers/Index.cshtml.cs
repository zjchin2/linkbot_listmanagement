using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Linkbot.Customers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Volo.Abp;

namespace Linkbot.Web.Pages.Customers
{
    public class IndexModel : LinkbotPageModel
    {
        private readonly ICustomerDetailsAppService _customerDetailsAppService;
        private readonly ICustomerSettingsAppService _customerSettingsAppService;
        private readonly ICustomerCustomPropertyAppService _customerCustomPropertyAppService;

        [BindProperty]
        public string Filter { get; set; }

        [BindProperty]
        public string FilterHidden { get; set; }

        public IndexModel(ICustomerDetailsAppService customerDetailsAppService
            , ICustomerSettingsAppService customerSettingsAppService
            , ICustomerCustomPropertyAppService customerCustomPropertysAppService)
        {
            _customerDetailsAppService = customerDetailsAppService;
            _customerSettingsAppService = customerSettingsAppService;
            _customerCustomPropertyAppService = customerCustomPropertysAppService;
        }

        #region Import/Export Excel
        public async Task<IActionResult> OnPostExportAsync()
        {
            try
            {
                var customers = await _customerDetailsAppService.GetListExportAsync(new PageLookupInputDto()
                {
                    Filter = FilterHidden
                }); ;

                var customPropertyList = await _customerCustomPropertyAppService.GetCustomerCustomPropertyAsync();

                var memoryStream = new MemoryStream();
                using (var workbook = new XLWorkbook())
                {
                    Dictionary<string, int> customDic = new Dictionary<string, int>();
                    var worksheet = workbook.Worksheets.Add("Customers Sheet");

                    var firstName = worksheet.Cell(1, 1);
                    var lastName = worksheet.Cell(1, 2);
                    var phoneNumber = worksheet.Cell(1, 3);
                    var email = worksheet.Cell(1, 4);
                    var customerStatus = worksheet.Cell(1, 5);
                    var substription = worksheet.Cell(1, 6);
                    var mobileNumber = worksheet.Cell(1, 7);
                    var addressLine1 = worksheet.Cell(1, 8);
                    var addressLine2 = worksheet.Cell(1, 9);
                    var postalCode = worksheet.Cell(1, 10);
                    var companyName = worksheet.Cell(1, 11);
                    var remark = worksheet.Cell(1, 12);

                    for (var i = 0; i < customPropertyList.Count; i++)
                    {
                        customDic.Add(customPropertyList[i].CustomPropertyName, 13 + i);
                        worksheet.Cell(1, 13 + i).Value = customPropertyList[i].CustomPropertyName;
                        worksheet.Cell(1, 13 + i).Style.Font.Bold = true;
                    }

                    firstName.Value = "First name";
                    firstName.Style.Font.Bold = true;
                    lastName.Value = "Last name";
                    lastName.Style.Font.Bold = true;
                    phoneNumber.Value = "Phone No.";
                    phoneNumber.Style.Font.Bold = true;
                    email.Value = "Email address";
                    email.Style.Font.Bold = true;
                    customerStatus.Value = "Status";
                    customerStatus.Style.Font.Bold = true;
                    substription.Value = "Substription";
                    substription.Style.Font.Bold = true;
                    mobileNumber.Value = "Mobile";
                    mobileNumber.Style.Font.Bold = true;
                    addressLine1.Value = "Address 1";
                    addressLine1.Style.Font.Bold = true;
                    addressLine2.Value = "Address 2";
                    addressLine2.Style.Font.Bold = true;
                    postalCode.Value = "PostalCode";
                    postalCode.Style.Font.Bold = true;
                    companyName.Value = "Company Name";
                    companyName.Style.Font.Bold = true;
                    remark.Value = "Remark";
                    remark.Style.Font.Bold = true;

                    for (var i = 0; i < customers.Count; i++)
                    {
                        var customer = customers[i];

                        worksheet.Cell(i + 2, 1).Value = customer.FirstName;
                        worksheet.Cell(i + 2, 2).Value = customer.LastName;
                        worksheet.Cell(i + 2, 3).Value = customer.PhoneNumber;
                        worksheet.Cell(i + 2, 4).Value = customer.EmailAddress;
                        worksheet.Cell(i + 2, 5).Value = customer.CustomerStatus;
                        worksheet.Cell(i + 2, 6).Value = customer.Subscription;
                        worksheet.Cell(i + 2, 7).Value = customer.MobileNumber;
                        worksheet.Cell(i + 2, 8).Value = customer.AddressLine1;
                        worksheet.Cell(i + 2, 9).Value = customer.AddressLine2;
                        worksheet.Cell(i + 2, 10).Value = customer.PostalCode;
                        worksheet.Cell(i + 2, 11).Value = customer.CompanyName;
                        worksheet.Cell(i + 2, 12).Value = customer.Remark;

                        foreach (var customProperty in customPropertyList)
                        {
                            customDic.TryGetValue(customProperty.CustomPropertyName, out int cellColumn);
                            customer.ExtraProperties.TryGetValue(customProperty.CustomPropertyName, out object extraValue);

                            if (extraValue != null)
                                worksheet.Cell(i + 2, cellColumn).Value = extraValue;
                        }
                    }
                    workbook.SaveAs(memoryStream);
                }
              
                memoryStream.Position = 0;
                return File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Customer.xlsx");
            }
            catch(Exception ex)
            {
                Logger.LogError($"Unexpected Error When Export Customer: {ex.Message}");
                Alerts.Warning("Unexpected Error While Export Customer Details");

                return await Task.FromResult<IActionResult>(Page());
            }
        }

        public async Task<IActionResult> OnPostImportAsync(IFormFile excel)
        {
            if (excel == null)
            {
                return await Task.FromResult<IActionResult>(Page());
            }

            using (var workbook = new XLWorkbook(excel.OpenReadStream()))
            {
                var worksheet = workbook.Worksheet("Customers Sheet");

                var count = 0;
                var errorRow = "";
                foreach (var row in worksheet.Rows())
                {
                    CreateUpdateCustomerDetailsDto input;
                    count += 1;
                    if (count > 1) //skip the first row.
                    {
                        input = new CreateUpdateCustomerDetailsDto()
                        {
                            FirstName = row.Cell(1).Value.ToString() == "" ? null : row.Cell(1).Value.ToString(),
                            LastName = row.Cell(2).Value.ToString() == "" ? null : row.Cell(2).Value.ToString(),
                            PhoneNumber = row.Cell(3).Value.ToString() == "" ? null : row.Cell(3).Value.ToString(),
                            EmailAddress = row.Cell(4).Value.ToString() == "" ? null : row.Cell(4).Value.ToString(),
                            CustomerStatus = row.Cell(5).Value.ToString() == "" ? null : row.Cell(5).Value.ToString(),
                            Subscription = row.Cell(6).Value.ToString() == "" ? null : row.Cell(6).Value.ToString(),
                            MobileNumber = row.Cell(7).Value.ToString() == "" ? null : row.Cell(7).Value.ToString(),
                            AddressLine1 = row.Cell(8).Value.ToString() == "" ? null : row.Cell(8).Value.ToString(),
                            AddressLine2 = row.Cell(9).Value.ToString() == "" ? null : row.Cell(9).Value.ToString(),
                            PostalCode = row.Cell(10).Value.ToString() == "" ? null : row.Cell(10).Value.ToString(),
                            CompanyName = row.Cell(11).Value.ToString() == "" ? null : row.Cell(11).Value.ToString(),
                            Remark = row.Cell(12).Value.ToString() == "" ? null : row.Cell(12).Value.ToString(),
                        };

                        var customPropertyList = _customerCustomPropertyAppService.GetCustomerCustomPropertyAsync().Result;

                        for (var i = 0; i < customPropertyList.Count; i++)
                        {
                            input.ExtraProperties.Add(worksheet.Row(1).Cell(13 + i).Value.ToString(), row.Cell(13 + i).Value.ToString());
                        }

                        try
                        {
                            var customerSettingsDto = await _customerSettingsAppService.GetCustomerSettingsAsync();

                            if ((customerSettingsDto.IsEmailMandatory && string.IsNullOrEmpty(input.EmailAddress)) ||
                                (customerSettingsDto.IsMobileNumberMandatory && string.IsNullOrEmpty(input.MobileNumber)))
                            {
                                if (errorRow == "")
                                    errorRow += count;
                                else
                                    errorRow += ", " + count;
                            }
                            else
                                await _customerDetailsAppService.CreateAsync(input);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError($"Invalid Row [{count}] When Import Customer: {ex.Message}");
                            if(errorRow == "")
                                errorRow += count;
                            else
                                errorRow += ", " + count ;
                        }
                    }
                }

                if (errorRow != "")
                    Alerts.Warning($"Row Number: [{errorRow}] unable to import.");

                return await Task.FromResult<IActionResult>(Page());
            }
        }
        #endregion
    }
}
