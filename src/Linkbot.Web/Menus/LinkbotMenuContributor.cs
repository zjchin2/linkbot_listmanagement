﻿using System.Threading.Tasks;
using Linkbot.Localization;
using Linkbot.MultiTenancy;
using Linkbot.Permissions;
using Volo.Abp.Identity.Web.Navigation;
using Volo.Abp.SettingManagement.Web.Navigation;
using Volo.Abp.TenantManagement.Web.Navigation;
using Volo.Abp.UI.Navigation;

namespace Linkbot.Web.Menus
{
    public class LinkbotMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            var administration = context.Menu.GetAdministration();
            var l = context.GetLocalizer<LinkbotResource>();

            context.Menu.Items.Insert(
                0,
                new ApplicationMenuItem(
                    LinkbotMenus.Home,
                    l["Menu:Home"],
                    "~/",
                    icon: "fas fa-home",
                    order: 0
                )
            );

            var linkbotMenu = new ApplicationMenuItem(
                "Customers",
                l["Menu:Customers"],
                icon: "fa fa-book"
            );

            context.Menu.AddItem(linkbotMenu);

            if (await context.IsGrantedAsync(LinkbotPermissions.Customers.Default))
            {
                linkbotMenu.AddItem(new ApplicationMenuItem(
                    "Customers.List",
                    l["Menu:CustomerList"],
                    url: "/Customers"
                ));
            }

            if (await context.IsGrantedAsync(LinkbotPermissions.CustomerSettings.Default))
            {
                linkbotMenu.AddItem(new ApplicationMenuItem(
                    "Customers.Setting",
                    l["Menu:CustomerSetting"],
                    url: "/Customers/Setting"
                ));
            }

            if (MultiTenancyConsts.IsEnabled)
            {
                administration.SetSubItemOrder(TenantManagementMenuNames.GroupName, 1);
            }
            else
            {
                administration.TryRemoveMenuItem(TenantManagementMenuNames.GroupName);
            }

            administration.SetSubItemOrder(IdentityMenuNames.GroupName, 2);
            administration.SetSubItemOrder(SettingManagementMenuNames.GroupName, 3);
        }
    }
}
