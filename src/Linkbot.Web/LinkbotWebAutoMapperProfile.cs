﻿using AutoMapper;
using Linkbot.Customers;

namespace Linkbot.Web
{
    public class LinkbotWebAutoMapperProfile : Profile
    {
        public LinkbotWebAutoMapperProfile()
        {
            //Define your AutoMapper configuration here for the Web project.
            CreateMap<CustomerDetailsDto, CreateUpdateCustomerDetailsDto>();

            CreateMap<CustomerSettingsDto, CreateUpdateCustomerSettingsDto>();

            CreateMap<CustomerCustomPropertyDto, CreateUpdateCustomPropertyDetailsDto>();

        }
    }
}
