﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace Linkbot.Web
{
    [Dependency(ReplaceServices = true)]
    public class LinkbotBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "Linkbot";

        public override string LogoUrl => "images/logo/logo.png";
    }
}
