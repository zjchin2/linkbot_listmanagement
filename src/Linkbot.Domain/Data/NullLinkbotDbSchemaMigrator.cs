﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Linkbot.Data
{
    /* This is used if database provider does't define
     * ILinkbotDbSchemaMigrator implementation.
     */
    public class NullLinkbotDbSchemaMigrator : ILinkbotDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}