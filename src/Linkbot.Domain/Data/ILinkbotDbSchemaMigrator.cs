﻿using System.Threading.Tasks;

namespace Linkbot.Data
{
    public interface ILinkbotDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
