﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Linkbot.Appointment
{
    public interface IAppointmentHistoryRepository : IRepository<AppointmentHistory, Guid>
    {
        Task<List<AppointmentHistory>> GetListAsync(
           string appointmentId = null,
           string customerId = null,
           int maxResultCount = int.MaxValue,
           int skipCount = 0,
           string sorting = null,
           CancellationToken cancellationToken = default);

        Task<long> GetCountAsync(
           string appointmentId = null,
           string customerId = null,
            CancellationToken cancellationToken = default);
    }
}
