﻿using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Linkbot.Appointment
{
    public class AppointmentHistory : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }
        public Guid CustomerId { get; set; }
        public string AppointmentName { get; set; }
        public string AppointmentStatus { get; set; }
        public string AppointmentId { get; set; }
        public DateTimeOffset AppointmentDate { get; set; }
        public DateTimeOffset BookingDate { get; set; }
        public DateTimeOffset CancellationDate { get; set; }
    }
}