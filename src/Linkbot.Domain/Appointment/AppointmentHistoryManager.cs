﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Linkbot.Appointment
{
    public class AppointmentHistoryManager : DomainService
    {
        private readonly IAppointmentHistoryRepository _appointmentHistoryRepository;

        public AppointmentHistoryManager(IAppointmentHistoryRepository appointmentHistoryRepository)
        {
            _appointmentHistoryRepository = appointmentHistoryRepository;
        }
    }
}
