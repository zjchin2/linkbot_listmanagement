﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Linkbot.Transaction
{
    public interface ITransactionHistoryRepository : IRepository<TransactionHistory, Guid>
    {
        Task<List<TransactionHistory>> GetListAsync(
           string transactionNo = null,
           string customerId = null,
           int maxResultCount = int.MaxValue,
           int skipCount = 0,
           string sorting = null,
           CancellationToken cancellationToken = default);

        Task<long> GetCountAsync(
            string transactionNo = null,
            string customerId = null,
            CancellationToken cancellationToken = default);
    }
}
