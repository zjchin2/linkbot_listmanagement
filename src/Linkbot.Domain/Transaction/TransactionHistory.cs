﻿using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Linkbot.Transaction
{
    public class TransactionHistory : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }
        public Guid CustomerId { get; set; }
        public string PurchaseItem { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public double Amount { get; set; }
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string TransactionNo { get; set; }
        public string Ref { get; set; }
        public string ItemCategory { get; set; }
    }
}