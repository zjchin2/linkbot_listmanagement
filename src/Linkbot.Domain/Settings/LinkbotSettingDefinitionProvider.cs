﻿using Volo.Abp.Settings;

namespace Linkbot.Settings
{
    public class LinkbotSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(LinkbotSettings.MySetting1));
        }
    }
}
