﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Linkbot.Customers
{
    public class CustomerSettings : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        public bool IsEmailMandatory { get; set; } = true;

        public bool IsMobileNumberMandatory { get; set; } = false;
    }
}
