﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Services;

namespace Linkbot.Customers
{
    public class CustomerManager : DomainService
    {
        private readonly ICustomerDetailsRepository _customerDetailsRepository;

        public CustomerManager(ICustomerDetailsRepository customerDetailsRepository)
        {
            _customerDetailsRepository = customerDetailsRepository;
        }

        public async Task<CustomerDetails> CreateAsync(CustomerDetails customerDetails)
        {
            if(customerDetails.MobileNumber != null)
            {
                var existingCustomer = await _customerDetailsRepository.GetListAsync(c => c.MobileNumber == customerDetails.MobileNumber);

                if (existingCustomer.Count() > 0)
                {
                    throw new CustomerAlreadyExistsException("mobile number", customerDetails.MobileNumber);
                }
            }

            if (customerDetails.EmailAddress != null)
            {
                var existingCustomer = await  _customerDetailsRepository.GetListAsync(c => c.EmailAddress == customerDetails.EmailAddress);

                if (existingCustomer.Count() > 0)
                {
                    throw new CustomerAlreadyExistsException("email address", customerDetails.EmailAddress);
                }
            }

            customerDetails.TenantId = CurrentTenant.Id;

            return customerDetails;
        }

        public async Task ChangeMobileNumberAsync(CustomerDetails customerDetails, string newMobileNumber)
        {
            if (newMobileNumber != null)
            {
                var existingCustomer = await _customerDetailsRepository.GetListAsync(c => c.MobileNumber == newMobileNumber);

                if (existingCustomer.Count() > 0)
                {
                    throw new CustomerAlreadyExistsException("mobile number", newMobileNumber);
                }
            }

            customerDetails.MobileNumber = newMobileNumber;
        }

        public async Task ChangeEmailAddressAsync(CustomerDetails customerDetails, string newEmailAddress)
        {
            if (newEmailAddress != null)
            {
                var existingCustomer = await _customerDetailsRepository.GetListAsync(c => c.EmailAddress == newEmailAddress);

                if (existingCustomer.Count() > 0)
                {
                    throw new CustomerAlreadyExistsException("email address", newEmailAddress);
                }
            }

            customerDetails.EmailAddress = newEmailAddress;
        }

        public CustomerDetails FindByMandatoryField(string fieldName, string fieldValue)
        {
            var customerDetails = new CustomerDetails();

            if (fieldName == nameof(customerDetails.MobileNumber))
            {
                customerDetails = _customerDetailsRepository.FirstOrDefault(c => c.MobileNumber == fieldValue);
            }
            else if (fieldName == nameof(customerDetails.EmailAddress))
            {
                customerDetails = _customerDetailsRepository.FirstOrDefault(c => c.EmailAddress == fieldValue);
            }

            if(customerDetails == null)
                throw new UserFriendlyException(message:$"Customer {fieldName}:'{fieldValue}' not found!");
  
            return customerDetails;
        }
    }
}
