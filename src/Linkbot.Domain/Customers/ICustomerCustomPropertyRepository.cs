﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Linkbot.Customers
{
    public interface ICustomerCustomPropertyRepository : IRepository<CustomerCustomProperty, Guid>
    {
    }
}
