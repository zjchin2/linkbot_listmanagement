﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Linkbot.Customers
{
    public class CustomerCustomProperty : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }
        public string CustomPropertyType { get; set; }
        public string CustomPropertyName { get; set; }
        public bool IsHostProperty { get; set; }
    }
}
