﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Linkbot.Customers
{
    public class CustomerDetailsDataSeederContributor
        : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<CustomerDetails, Guid> _customerDetailsRepository;

        public CustomerDetailsDataSeederContributor(IRepository<CustomerDetails, Guid> customerDetailsRepository)
        {
            _customerDetailsRepository = customerDetailsRepository;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            //if (await _customerDetailsRepository.GetCountAsync() <= 0)
            //{
            //    await _customerDetailsRepository.InsertAsync(
            //        new CustomerDetails
            //        {
            //            FirstName = "Eddie",
            //            LastName = "Chin",
            //            PhoneNumber = "92411195",
            //            MobileNumber = "92411195",
            //            EmailAddress = "eddie.chin@linkbot.sg",
            //            AddressLine1 = "524B Tampines Central 7",
            //            AddressLine2 = "#14-69",
            //            PostalCode = "522524"
            //        },
            //        autoSave: true
            //    );

            //    await _customerDetailsRepository.InsertAsync(
            //        new CustomerDetails
            //        {
            //            FirstName = "Kai",
            //            LastName = "Chua",
            //            PhoneNumber = "91696758",
            //            MobileNumber = "91696758",
            //            EmailAddress = "kai.chua@linkbot.sg",
            //            AddressLine1 = "317 Tampines Street 33",
            //            AddressLine2 = "#04-66",
            //            PostalCode = "520317"
            //        },
            //        autoSave: true
            //    );
            //}
        }
    }
}
