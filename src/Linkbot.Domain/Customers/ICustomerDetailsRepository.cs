﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Linkbot.Customers
{
    public interface ICustomerDetailsRepository : IRepository<CustomerDetails, Guid>
    {
        Task<List<CustomerDetails>> GetListAsync(
           string filter = null,
           int maxResultCount = int.MaxValue,
           int skipCount = 0,
           string sorting = null,
           CancellationToken cancellationToken = default);

        Task<long> GetCountAsync(
            string filter = null,
            CancellationToken cancellationToken = default);

        Task UpdateExtraPropertyAsync(
            string inputValueBefore,
            string inputValueAfter,
            CancellationToken cancellationToken = default);
    }
}
