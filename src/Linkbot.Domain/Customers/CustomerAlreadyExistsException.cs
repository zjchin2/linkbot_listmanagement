﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;

namespace Linkbot.Customers
{
    public class CustomerAlreadyExistsException : BusinessException
    {
        public CustomerAlreadyExistsException(string dataName, string dataValue)
            : base(LinkbotDomainErrorCodes.CustomerAlreadyExists)
        {
            WithData("fieldName", dataName);
            WithData("fieldValue", dataValue);
        }
    }
}
