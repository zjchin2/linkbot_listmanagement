﻿using Linkbot.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Linkbot.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class LinkbotController : AbpController
    {
        protected LinkbotController()
        {
            LocalizationResource = typeof(LinkbotResource);
        }
    }
}