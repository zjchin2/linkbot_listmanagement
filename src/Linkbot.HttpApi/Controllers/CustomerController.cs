﻿using Linkbot.Customers;
using Linkbot.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Linkbot.Controllers
{
    /* Inherit your controllers from this class.
     */

    [RemoteService(Name = "Customers")]
    [Area("Customer")]
    [Route("api/app/customerdetails")]
    public class CustomerController : AbpController, ICustomerDetailsAppService
    {
        protected ICustomerDetailsAppService CustomerDetailsAppService { get; }

        public CustomerController(ICustomerDetailsAppService customerDetailsAppService)
        {
            CustomerDetailsAppService = customerDetailsAppService;
        }

        [HttpPost]
        public Task<CustomerDetailsDto> CreateAsync(CreateUpdateCustomerDetailsDto input)
        {
            return CustomerDetailsAppService.CreateAsync(input);
        }

        [HttpPut]
        public Task<CustomerDetailsDto> UpdateAsync(Guid id, CreateUpdateCustomerDetailsDto input)
        {
            return CustomerDetailsAppService.UpdateAsync(id, input);
        }

        [HttpGet]
        [Route("getList")]
        public Task<PagedResultDto<CustomerDetailsDto>> GetListAsync(PageLookupInputDto input)
        {
            return CustomerDetailsAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("export")]
        public Task<List<CustomerDetailsDto>> GetListExportAsync(PageLookupInputDto input)
        {
            return CustomerDetailsAppService.GetListExportAsync(input);
        }

        [HttpDelete]
        [Route("{id}")]
        public Task DeleteAsync(Guid id)
        {
            return CustomerDetailsAppService.DeleteAsync(id);
        }

        [HttpGet]
        [Route("{id}")]
        public Task<CustomerDetailsDto> GetAsync(Guid id)
        {
            return CustomerDetailsAppService.GetAsync(id);
        }
    }
}