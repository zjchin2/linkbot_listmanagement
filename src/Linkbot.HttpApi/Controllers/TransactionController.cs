﻿using Linkbot.Localization;
using Linkbot.Transaction;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Linkbot.Controllers
{
    /* Inherit your controllers from this class.
     */

    [RemoteService(Name = "Transaction")]
    [Area("Transaction")]
    [Route("api/app/transaction")]
    public class TransactionController : AbpController, ITransactionHistoryAppService
    {
        protected ITransactionHistoryAppService TransactionHistoryAppService { get; }

        public TransactionController(ITransactionHistoryAppService transactionHistoryAppService)
        {
            TransactionHistoryAppService = transactionHistoryAppService;
        }

        [HttpPost]
        public Task<TransactionHistoryDto> CreateAsync(CreateUpdateTransactionHistoryDto input)
        {
            return TransactionHistoryAppService.CreateAsync(input);
        }

        [HttpPut]
        public Task<TransactionHistoryDto> UpdateAsync(Guid id, CreateUpdateTransactionHistoryDto input)
        {
            return TransactionHistoryAppService.UpdateAsync(id, input);
        }

        [HttpGet]
        [Route("getList")]
        public Task<PagedResultDto<TransactionHistoryDto>> GetListAsync(GetTransactionHistoryInput input)
        {
            return TransactionHistoryAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("getPurchaseHistoryList")]
        public Task<PagedResultDto<CreateUpdateTransactionHistoryDto>> GetPurchaseHistoryAsync(PageLookupInputDto input)
        {
            return TransactionHistoryAppService.GetPurchaseHistoryAsync(input);
        }

        [HttpDelete]
        [Route("{id}")]
        public Task DeleteAsync(Guid id)
        {
            return TransactionHistoryAppService.DeleteAsync(id);
        }

        [HttpGet]
        [Route("{id}")]
        public Task<TransactionHistoryDto> GetAsync(Guid id)
        {
            return TransactionHistoryAppService.GetAsync(id);
        }
    }
}