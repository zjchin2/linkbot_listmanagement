﻿using Linkbot.Appointment;
using Linkbot.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.AspNetCore.Mvc;

namespace Linkbot.Controllers
{
    /* Inherit your controllers from this class.
     */

    [RemoteService(Name = "Appointment")]
    [Area("Appointment")]
    [Route("api/app/appointment")]
    public class AppointmentController : AbpController, IAppointmentHistoryAppService
    {
        protected IAppointmentHistoryAppService AppointmentHistoryAppService { get; }

        public AppointmentController(IAppointmentHistoryAppService appointmentHistoryAppService)
        {
            AppointmentHistoryAppService = appointmentHistoryAppService;
        }

        [HttpPost]
        public Task<AppointmentHistoryDto> CreateAsync(CreateUpdateAppointmentHistoryDto input)
        {
            return AppointmentHistoryAppService.CreateAsync(input);
        }

        [HttpPut]
        public Task<AppointmentHistoryDto> UpdateAsync(Guid id, CreateUpdateAppointmentHistoryDto input)
        {
            return AppointmentHistoryAppService.UpdateAsync(id, input);
        }

        [HttpGet]
        [Route("getList")]
        public Task<PagedResultDto<AppointmentHistoryDto>> GetListAsync(GetAppointmentHistoryInput input)
        {
            return AppointmentHistoryAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("getUpcomingList")]
        public Task<PagedResultDto<CreateUpdateAppointmentHistoryDto>> GetUpcomingApptListPageAsync(PageLookupInputDto input)
        {
            return AppointmentHistoryAppService.GetUpcomingApptListPageAsync(input);
        }

        [HttpDelete]
        [Route("{id}")]
        public Task DeleteAsync(Guid id)
        {
            return AppointmentHistoryAppService.DeleteAsync(id);
        }

        [HttpGet]
        [Route("{id}")]
        public Task<AppointmentHistoryDto> GetAsync(Guid id)
        {
            return AppointmentHistoryAppService.GetAsync(id);
        }
    }
}