﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Linkbot.Migrations
{
    public partial class Add_CustomerDetails_MobileNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsPhoneNumberMandatory",
                table: "Linkbot_CustomerSettings",
                newName: "IsMobileNumberMandatory");

            migrationBuilder.AddColumn<string>(
                name: "MobileNumber",
                table: "Linkbot_CustomerDetails",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MobileNumber",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.RenameColumn(
                name: "IsMobileNumberMandatory",
                table: "Linkbot_CustomerSettings",
                newName: "IsPhoneNumberMandatory");
        }
    }
}
