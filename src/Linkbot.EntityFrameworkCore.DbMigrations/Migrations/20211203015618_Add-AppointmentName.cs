﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Linkbot.Migrations
{
    public partial class AddAppointmentName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AppointmentName",
                table: "Linkbot_AppointmentHistory",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppointmentName",
                table: "Linkbot_AppointmentHistory");
        }
    }
}
