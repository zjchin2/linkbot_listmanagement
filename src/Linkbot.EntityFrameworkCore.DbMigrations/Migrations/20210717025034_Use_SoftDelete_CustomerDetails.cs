﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Linkbot.Migrations
{
    public partial class Use_SoftDelete_CustomerDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DeleterId",
                table: "Linkbot_CustomerDetails",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Linkbot_CustomerDetails",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Linkbot_CustomerDetails",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeleterId",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Linkbot_CustomerDetails");
        }
    }
}
