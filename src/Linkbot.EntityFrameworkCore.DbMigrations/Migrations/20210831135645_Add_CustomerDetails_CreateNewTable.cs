﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Linkbot.Migrations
{
    public partial class Add_CustomerDetails_CreateNewTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Linkbot_CustomerDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerStatus",
                table: "Linkbot_CustomerDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Remark",
                table: "Linkbot_CustomerDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Subscription",
                table: "Linkbot_CustomerDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Exceptions",
                table: "AbpAuditLogs",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(4000)",
                oldMaxLength: 4000,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Linkbot_AppointmentHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    TenantId = table.Column<Guid>(type: "uuid", nullable: true),
                    CustomerId = table.Column<Guid>(type: "uuid", nullable: false),
                    AppointmentStatus = table.Column<string>(type: "text", nullable: true),
                    AppointmentId = table.Column<string>(type: "text", nullable: true),
                    AppointmentDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    BookingDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CancellationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ExtraProperties = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uuid", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    DeleterId = table.Column<Guid>(type: "uuid", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Linkbot_AppointmentHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Linkbot_TransactionHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    TenantId = table.Column<Guid>(type: "uuid", nullable: true),
                    CustomerId = table.Column<Guid>(type: "uuid", nullable: false),
                    PurchaseItem = table.Column<string>(type: "text", nullable: true),
                    TransactionDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Amount = table.Column<double>(type: "double precision", nullable: false),
                    ExtraProperties = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uuid", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    DeleterId = table.Column<Guid>(type: "uuid", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Linkbot_TransactionHistory", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                    name: "FK_TrxHistory_CustDetails_CustId",
                    table: "Linkbot_TransactionHistory",
                    column: "CustomerId",
                    principalTable: "Linkbot_CustomerDetails",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                    name: "FK_ApptHistory_CustDetails_CustId",
                    table: "Linkbot_AppointmentHistory",
                    column: "CustomerId",
                    principalTable: "Linkbot_CustomerDetails",
                    principalColumn: "Id",
                    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Linkbot_AppointmentHistory");

            migrationBuilder.DropTable(
                name: "Linkbot_TransactionHistory");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.DropColumn(
                name: "CustomerStatus",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.DropColumn(
                name: "Remark",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.DropColumn(
                name: "Subscription",
                table: "Linkbot_CustomerDetails");

            migrationBuilder.AlterColumn<string>(
                name: "Exceptions",
                table: "AbpAuditLogs",
                type: "character varying(4000)",
                maxLength: 4000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }
    }
}
