﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Linkbot.Migrations
{
    public partial class Add_TransactionHistory_Properties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Linkbot_TransactionHistory",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Ref",
                table: "Linkbot_TransactionHistory",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransactionNo",
                table: "Linkbot_TransactionHistory",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "UnitPrice",
                table: "Linkbot_TransactionHistory",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Linkbot_TransactionHistory");

            migrationBuilder.DropColumn(
                name: "Ref",
                table: "Linkbot_TransactionHistory");

            migrationBuilder.DropColumn(
                name: "TransactionNo",
                table: "Linkbot_TransactionHistory");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "Linkbot_TransactionHistory");
        }
    }
}
