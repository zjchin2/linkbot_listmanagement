﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace Linkbot.EntityFrameworkCore
{
    [DependsOn(
        typeof(LinkbotEntityFrameworkCoreModule)
        )]
    public class LinkbotEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<LinkbotMigrationsDbContext>();
        }
    }
}
