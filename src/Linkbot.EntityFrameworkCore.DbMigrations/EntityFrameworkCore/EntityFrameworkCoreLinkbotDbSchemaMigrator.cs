﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Linkbot.Data;
using Volo.Abp.DependencyInjection;

namespace Linkbot.EntityFrameworkCore
{
    public class EntityFrameworkCoreLinkbotDbSchemaMigrator
        : ILinkbotDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreLinkbotDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the LinkbotMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<LinkbotMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}