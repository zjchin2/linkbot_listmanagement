﻿using Volo.Abp.Localization;

namespace Linkbot.Localization
{
    [LocalizationResourceName("Linkbot")]
    public class LinkbotResource
    {

    }
}