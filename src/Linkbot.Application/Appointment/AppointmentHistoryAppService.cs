﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Linkbot.Permissions;
using Microsoft.AspNetCore.Authorization;
using Linkbot.Customers;
using Volo.Abp;

namespace Linkbot.Appointment
{
    [Authorize(LinkbotPermissions.AppointmentHistory.Default)]
    public class AppointmentHistoryAppService :
        CrudAppService<
            AppointmentHistory,
            AppointmentHistoryDto,
            Guid,
            GetAppointmentHistoryInput,
            CreateUpdateAppointmentHistoryDto>,
        IAppointmentHistoryAppService
    {
        private readonly IAppointmentHistoryRepository _appointmentHistoryRepository;
        private readonly AppointmentHistoryManager _appointmentHistoryManager;
        private readonly CustomerManager _customerManager;

        public AppointmentHistoryAppService(IRepository<AppointmentHistory, Guid> repository,
            IAppointmentHistoryRepository appointmentHistoryRepository,
            AppointmentHistoryManager appointmentHistoryManager,
            CustomerManager customerManager)
            : base(repository)
        {
            GetPolicyName = LinkbotPermissions.AppointmentHistory.Default;
            GetListPolicyName = LinkbotPermissions.AppointmentHistory.Default;
            CreatePolicyName = LinkbotPermissions.AppointmentHistory.Create;
            UpdatePolicyName = LinkbotPermissions.AppointmentHistory.Edit;

            _appointmentHistoryRepository = appointmentHistoryRepository;
            _appointmentHistoryManager = appointmentHistoryManager;
            _customerManager = customerManager;
        }

        [Authorize(LinkbotPermissions.AppointmentHistory.Create)]
        public override async Task<AppointmentHistoryDto> CreateAsync(CreateUpdateAppointmentHistoryDto input)
        {
            var customerDetails = new CustomerDetails();

            if (!string.IsNullOrEmpty(input.MobileNumber))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.MobileNumber), input.MobileNumber);
            }
            else if (!string.IsNullOrEmpty(input.EmailAddress))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.EmailAddress), input.EmailAddress);
            }
            else
            {
                throw new UserFriendlyException(message: $"Please provide mobile number or email address.");
            }

            var appointment = ObjectMapper.Map<CreateUpdateAppointmentHistoryDto, AppointmentHistory>(input);
            appointment.CustomerId = customerDetails.Id;
            appointment.TenantId = customerDetails.TenantId;

            await _appointmentHistoryRepository.InsertAsync(appointment);

            return ObjectMapper.Map<AppointmentHistory, AppointmentHistoryDto>(appointment);
        }

        [Authorize(LinkbotPermissions.AppointmentHistory.Default)]
        public override async Task<PagedResultDto<AppointmentHistoryDto>> GetListAsync(GetAppointmentHistoryInput input)
        {
            var customerDetails = new CustomerDetails();

            if (!string.IsNullOrEmpty(input.MobileNumber))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.MobileNumber), input.MobileNumber);
            }
            else if (!string.IsNullOrEmpty(input.EmailAddress))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.EmailAddress), input.EmailAddress);
            }

            var list = await _appointmentHistoryRepository.GetListAsync(input.AppointmentId, customerDetails.Id == Guid.Empty ? null : customerDetails.Id.ToString(), input.MaxResultCount, input.SkipCount, input.Sorting);
            var count = await _appointmentHistoryRepository.GetCountAsync(input.AppointmentId, customerDetails.Id == Guid.Empty ? null : customerDetails.Id.ToString());

            return new PagedResultDto<AppointmentHistoryDto>(
                count,
                ObjectMapper.Map<List<AppointmentHistory>, List<AppointmentHistoryDto>>(list)
            );
        }

        [Authorize(LinkbotPermissions.AppointmentHistory.Default)]
        public virtual async Task<PagedResultDto<CreateUpdateAppointmentHistoryDto>> GetUpcomingApptListPageAsync(PageLookupInputDto input)
        {
            var list = (await _appointmentHistoryRepository.GetListAsync())
                        .Where(x => x.CustomerId == new Guid(input.Filter))
                        .Where(x => x.AppointmentDate.Date >= DateTimeOffset.Now.Date).ToList();

            return new PagedResultDto<CreateUpdateAppointmentHistoryDto>(
                list.Count,
                ObjectMapper.Map<List<AppointmentHistory>, List<CreateUpdateAppointmentHistoryDto>>(list)
            );
        }
    }
}
