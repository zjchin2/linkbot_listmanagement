﻿using Linkbot.Customers;
using AutoMapper;
using Linkbot.Transaction;
using System.Collections.Generic;
using Linkbot.Appointment;

namespace Linkbot
{
    public class LinkbotApplicationAutoMapperProfile : Profile
    {
        public LinkbotApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */

            CreateMap<CustomerDetails, CustomerDetailsDto>();
            CreateMap<CreateUpdateCustomerDetailsDto, CustomerDetails>();

            CreateMap<CustomerSettings, CustomerSettingsDto>();
            CreateMap<CreateUpdateCustomerSettingsDto, CustomerSettings>();

            CreateMap<CustomerCustomProperty, CustomerCustomPropertyDto>();
            CreateMap<CustomerCustomPropertyDto, CustomerCustomProperty>();
            CreateMap<CreateUpdateCustomPropertyDetailsDto, CustomerCustomPropertyDto>();
            CreateMap<CustomerCustomProperty, CreateUpdateCustomPropertyDetailsDto>();

            CreateMap<TransactionHistory, TransactionHistoryDto>();
            CreateMap<CreateUpdateTransactionHistoryDto, TransactionHistory>();
            CreateMap<TransactionHistory, CreateUpdateTransactionHistoryDto>();

            CreateMap<AppointmentHistory, AppointmentHistoryDto>();
            CreateMap<AppointmentHistory, CreateUpdateAppointmentHistoryDto>();
            CreateMap<CreateUpdateAppointmentHistoryDto, AppointmentHistory>();
        }
    }
}
