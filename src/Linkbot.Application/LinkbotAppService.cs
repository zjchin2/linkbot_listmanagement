﻿using System;
using System.Collections.Generic;
using System.Text;
using Linkbot.Localization;
using Volo.Abp.Application.Services;

namespace Linkbot
{
    /* Inherit your application services from this class.
     */
    public abstract class LinkbotAppService : ApplicationService
    {
        protected LinkbotAppService()
        {
            LocalizationResource = typeof(LinkbotResource);
        }
    }
}
