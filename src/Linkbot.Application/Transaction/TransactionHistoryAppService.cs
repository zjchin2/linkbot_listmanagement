﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Linkbot.Permissions;
using Microsoft.AspNetCore.Authorization;
using Linkbot.Customers;
using Volo.Abp;

namespace Linkbot.Transaction
{
    public class TransactionHistoryAppService :
        CrudAppService<
            TransactionHistory,
            TransactionHistoryDto,
            Guid,
            GetTransactionHistoryInput,
            CreateUpdateTransactionHistoryDto>, 
        ITransactionHistoryAppService
    {
        private readonly ITransactionHistoryRepository _transactionHistoryRepository;
        private readonly TransactionHistoryManager _transactionHistoryManager;
        private readonly CustomerManager _customerManager;

        public TransactionHistoryAppService(IRepository<TransactionHistory, Guid> repository,
            ITransactionHistoryRepository transactionHistoryRepository,
            TransactionHistoryManager transactionHistoryManager,
            CustomerManager customerManager)
            : base(repository)
        {
            GetPolicyName = LinkbotPermissions.TransactionHistory.Default;
            GetListPolicyName = LinkbotPermissions.TransactionHistory.Default;
            CreatePolicyName = LinkbotPermissions.TransactionHistory.Create;
            UpdatePolicyName = LinkbotPermissions.TransactionHistory.Edit;

            _transactionHistoryRepository = transactionHistoryRepository;
            _transactionHistoryManager = transactionHistoryManager;
            _customerManager = customerManager;
        }


        [Authorize(LinkbotPermissions.TransactionHistory.Create)]
        public override async Task<TransactionHistoryDto> CreateAsync(CreateUpdateTransactionHistoryDto input)
        {
            var customerDetails = new CustomerDetails();

            if (!string.IsNullOrEmpty(input.MobileNumber))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.MobileNumber), input.MobileNumber);
            }
            else if (!string.IsNullOrEmpty(input.EmailAddress))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.EmailAddress), input.EmailAddress);
            }
            else
            {
                throw new UserFriendlyException(message: $"Please provide mobile number or email address.");
            }

            var transaction = ObjectMapper.Map<CreateUpdateTransactionHistoryDto, TransactionHistory>(input);
            transaction.CustomerId = customerDetails.Id;
            transaction.TenantId = customerDetails.TenantId;

            await _transactionHistoryRepository.InsertAsync(transaction);

            return ObjectMapper.Map<TransactionHistory, TransactionHistoryDto>(transaction);
        }

        [Authorize(LinkbotPermissions.TransactionHistory.Default)]
        public override async Task<PagedResultDto<TransactionHistoryDto>> GetListAsync(GetTransactionHistoryInput input)
        {
            var customerDetails = new CustomerDetails();

            if (!string.IsNullOrEmpty(input.MobileNumber))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.MobileNumber), input.MobileNumber);
            }
            else if (!string.IsNullOrEmpty(input.EmailAddress))
            {
                customerDetails = _customerManager.FindByMandatoryField(nameof(customerDetails.EmailAddress), input.EmailAddress);
            }

            var list = await _transactionHistoryRepository.GetListAsync(input.TransactionNo, customerDetails.Id == Guid.Empty ? null : customerDetails.Id.ToString(), input.MaxResultCount, input.SkipCount, input.Sorting) ;
            var count = await _transactionHistoryRepository.GetCountAsync(input.TransactionNo, customerDetails.Id == Guid.Empty ? null : customerDetails.Id.ToString());

            return new PagedResultDto<TransactionHistoryDto>(
                count,
                ObjectMapper.Map<List<TransactionHistory>, List<TransactionHistoryDto>>(list)
            );
        }

        [Authorize(LinkbotPermissions.TransactionHistory.Default)]
        public virtual async Task<PagedResultDto<CreateUpdateTransactionHistoryDto>> GetPurchaseHistoryAsync(PageLookupInputDto input)
        {
            var list = (await _transactionHistoryRepository.GetListAsync())
                        .Where(x => x.CustomerId == new Guid(input.Filter)).ToList();

            return new PagedResultDto<CreateUpdateTransactionHistoryDto>(
                list.Count,
                ObjectMapper.Map<List<TransactionHistory>, List<CreateUpdateTransactionHistoryDto>>(list)
            );
        }
    }
}
