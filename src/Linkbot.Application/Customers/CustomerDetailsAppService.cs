﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Linkbot.Permissions;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Data;
using Volo.Abp.ObjectExtending;
using System.IO;
using Volo.Abp;

namespace Linkbot.Customers
{
    public class CustomerDetailsAppService :
        CrudAppService<
            CustomerDetails,
            CustomerDetailsDto,
            Guid,
            PageLookupInputDto,
            CreateUpdateCustomerDetailsDto>,
            ICustomerDetailsAppService
    {
        private readonly ICustomerDetailsRepository _customerDetailsRepository;
        private readonly CustomerManager _customerManager;

        public CustomerDetailsAppService(IRepository<CustomerDetails, Guid> repository,
            ICustomerDetailsRepository customerDetailsRepository,
            CustomerManager customerManager)
            : base(repository)
        {
            GetPolicyName = LinkbotPermissions.Customers.Default;
            GetListPolicyName = LinkbotPermissions.Customers.Default;
            CreatePolicyName = LinkbotPermissions.Customers.Create;
            UpdatePolicyName = LinkbotPermissions.Customers.Edit;
            DeletePolicyName = LinkbotPermissions.Customers.Delete;

            _customerDetailsRepository = customerDetailsRepository;
            _customerManager = customerManager;
        }


        [Authorize(LinkbotPermissions.Customers.Create)]
        public override async Task<CustomerDetailsDto> CreateAsync(CreateUpdateCustomerDetailsDto input)
        {
            var customer = ObjectMapper.Map<CreateUpdateCustomerDetailsDto, CustomerDetails>(input);

            customer = await _customerManager.CreateAsync(customer);

            await _customerDetailsRepository.InsertAsync(customer);
            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<CustomerDetails, CustomerDetailsDto>(customer);
        }

        [Authorize(LinkbotPermissions.Customers.Edit)]
        public override async Task<CustomerDetailsDto> UpdateAsync(Guid id, CreateUpdateCustomerDetailsDto input)
        {
            var customer = await _customerDetailsRepository.GetAsync(id);

            if(customer.MobileNumber != input.MobileNumber)
            {
                await _customerManager.ChangeMobileNumberAsync(customer, input.MobileNumber);
            }

            if (customer.EmailAddress != input.EmailAddress)
            {
                await _customerManager.ChangeEmailAddressAsync(customer, input.EmailAddress);
            }

            customer.FirstName = input.FirstName;
            customer.LastName = input.LastName;
            customer.EmailAddress = input.EmailAddress;
            customer.MobileNumber = input.MobileNumber;
            customer.AddressLine1 = input.AddressLine1;
            customer.AddressLine2 = input.AddressLine2;
            customer.PostalCode = input.PostalCode;
            customer.CompanyName = input.CompanyName;
            customer.CustomerStatus = input.CustomerStatus;
            customer.Subscription = input.Subscription;
            customer.Remark = input.Remark;
            customer.ExtraProperties = input.ExtraProperties;
            
            await _customerDetailsRepository.UpdateAsync(customer);

            return ObjectMapper.Map<CustomerDetails, CustomerDetailsDto>(customer);
        }

        [Authorize(LinkbotPermissions.Customers.Default)]
        public override async Task<PagedResultDto<CustomerDetailsDto>> GetListAsync(PageLookupInputDto input)
        {
            //TODO: Currently filter FirstName, email address and mobile number
            var list = await _customerDetailsRepository.GetListAsync(input.Filter, input.MaxResultCount, input.SkipCount, input.Sorting);
            var count = await _customerDetailsRepository.GetCountAsync(input.Filter);

            return new PagedResultDto<CustomerDetailsDto>(
                count,
                ObjectMapper.Map<List<CustomerDetails>, List<CustomerDetailsDto>>(list)
            );
        }

        public virtual async Task<List<CustomerDetailsDto>> GetListExportAsync(PageLookupInputDto input)
        {
            var list = await _customerDetailsRepository.GetListAsync(input.Filter, int.MaxValue);

            return new List<CustomerDetailsDto>(
                ObjectMapper.Map<List<CustomerDetails>, List<CustomerDetailsDto>>(list)
            );
        }
    }
}
