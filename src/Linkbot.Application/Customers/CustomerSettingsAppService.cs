﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Linkbot.Permissions;
using Microsoft.AspNetCore.Authorization;

namespace Linkbot.Customers
{
    public class CustomerSettingsAppService :
        CrudAppService<
            CustomerSettings,
            CustomerSettingsDto,
            Guid,
            PagedAndSortedResultRequestDto,
            CreateUpdateCustomerSettingsDto>,
        ICustomerSettingsAppService
    {
        private readonly ICustomerSettingsRepository _customerSettingsRepository;

        public CustomerSettingsAppService(IRepository<CustomerSettings, Guid> repository, ICustomerSettingsRepository customerSettingsRepository)
            : base(repository)
        {
            GetPolicyName = LinkbotPermissions.CustomerSettings.Default;
            GetListPolicyName = LinkbotPermissions.CustomerSettings.MandatoryFieldListing;
            CreatePolicyName = LinkbotPermissions.CustomerSettings.MandatoryFieldEdit;
            UpdatePolicyName = LinkbotPermissions.CustomerSettings.MandatoryFieldEdit;

            _customerSettingsRepository = customerSettingsRepository;
        }

        public async Task<CustomerSettingsDto> GetCustomerSettingsAsync()
        {
            var customerSettings = await _customerSettingsRepository.FirstOrDefaultAsync();

            if(customerSettings == null)
            {
                customerSettings = new CustomerSettings();
            }

            return ObjectMapper.Map<CustomerSettings, CustomerSettingsDto>(customerSettings);
        }

        [Authorize(LinkbotPermissions.CustomerSettings.MandatoryFieldEdit)]
        public async Task<CustomerSettingsDto> UpdateCustomerSettingsAsync(CreateUpdateCustomerSettingsDto input)
        {
            var customerSettings = await _customerSettingsRepository.FirstOrDefaultAsync();

            if (customerSettings == null)
            {
                customerSettings = ObjectMapper.Map<CreateUpdateCustomerSettingsDto, CustomerSettings>(input);
                customerSettings.TenantId = CurrentTenant.Id;
                await _customerSettingsRepository.InsertAsync(customerSettings);
            }
            else
            {
                customerSettings.IsEmailMandatory = input.IsEmailMandatory;
                customerSettings.IsMobileNumberMandatory = input.IsMobileNumberMandatory;
                await _customerSettingsRepository.UpdateAsync(customerSettings);
            }

            return ObjectMapper.Map<CustomerSettings, CustomerSettingsDto>(customerSettings);
        }
    }
}
