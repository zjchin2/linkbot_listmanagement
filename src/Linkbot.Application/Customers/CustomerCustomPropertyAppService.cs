﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Linkbot.Permissions;
using Microsoft.AspNetCore.Authorization;

namespace Linkbot.Customers
{
    public class CustomerCustomPropertyAppService :
        CrudAppService<
            CustomerCustomProperty,
            CustomerCustomPropertyDto,
            Guid,
            PagedAndSortedResultRequestDto,
            CustomerCustomPropertyDto>,
        ICustomerCustomPropertyAppService
    {
        private readonly ICustomerCustomPropertyRepository _customPropertyRepository;
        private readonly ICustomerDetailsRepository _customerDetailsRepository;
        private readonly IAuthorizationService _authorizationService;

        public CustomerCustomPropertyAppService(IRepository<CustomerCustomProperty, Guid> repository
            , ICustomerCustomPropertyRepository customerCustomPropertyRepository
            , ICustomerDetailsRepository customerDetailsRepository
            , IAuthorizationService authorizationService)
            : base(repository)
        {
            GetPolicyName = LinkbotPermissions.CustomerSettings.Default;
            GetListPolicyName = LinkbotPermissions.CustomerSettings.CustomPropertyListing;
            CreatePolicyName = LinkbotPermissions.CustomerSettings.CustomPropertyCreate;
            UpdatePolicyName = LinkbotPermissions.CustomerSettings.CustomPropertyEdit;
            DeletePolicyName = LinkbotPermissions.CustomerSettings.CustomPropertyDelete;

            _customPropertyRepository = customerCustomPropertyRepository;
            _customerDetailsRepository = customerDetailsRepository;
            _authorizationService = authorizationService;
        }

        /*
         * TODO: 
         * 1. Check unique custom property name
         * 2. Update customer detail when create new property
        */

        public virtual async Task<PagedResultDto<CreateUpdateCustomPropertyDetailsDto>> GetListPageAsync()
        {
            var list = new List<CustomerCustomProperty>();

            if(await _authorizationService.IsGrantedAsync(LinkbotPermissions.CustomerSettings.CustomPropertyHost))
            {
                list = (await _customPropertyRepository.GetListAsync()).ToList();
            }
            else
            {
                list = (await _customPropertyRepository.GetListAsync())
                            .Where(x => x.IsHostProperty == false).ToList();
            }

            return new PagedResultDto<CreateUpdateCustomPropertyDetailsDto>(
                list.Count,
                ObjectMapper.Map<List<CustomerCustomProperty>, List<CreateUpdateCustomPropertyDetailsDto>>(list)
            );
        }

        public virtual async Task<List<CreateUpdateCustomPropertyDetailsDto>> GetCustomerCustomPropertyAsync()
        {
            var customPropertyList = new List<CustomerCustomProperty>();

            if (await _authorizationService.IsGrantedAsync(LinkbotPermissions.CustomerSettings.CustomPropertyHost))
            {
                customPropertyList = (await _customPropertyRepository.GetListAsync()).ToList();
            }
            else
            {
                customPropertyList = (await _customPropertyRepository.GetListAsync())
                            .Where(x => x.IsHostProperty == false).ToList();
            }

            var cp = ObjectMapper.Map<List<CustomerCustomProperty>, List<CreateUpdateCustomPropertyDetailsDto>>(customPropertyList);
            return cp;
        }

        [Authorize(LinkbotPermissions.CustomerSettings.CustomPropertyEdit)]
        public async Task UpdateCustomPropertyAsync(Guid id, CustomerCustomPropertyDto input)
        {
            await CheckUpdatePolicyAsync();
            var entity = await GetEntityByIdAsync(id);
            var inputValueBefore = entity.CustomPropertyName;
            input.CustomPropertyType = entity.CustomPropertyType; //Note: prevent CustomPropertyType change from input

            await MapToEntityAsync(input, entity);
            await Repository.UpdateAsync(entity, autoSave: true);

            await _customerDetailsRepository.UpdateExtraPropertyAsync(inputValueBefore, input.CustomPropertyName);

        }
    }
}
