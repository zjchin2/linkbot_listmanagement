﻿using Linkbot.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Linkbot.Transaction
{
    public class EfCoreTransactionHistoryRepository
        : EfCoreRepository<LinkbotDbContext, TransactionHistory, Guid>,
            ITransactionHistoryRepository
    {
        public EfCoreTransactionHistoryRepository(
            IDbContextProvider<LinkbotDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
        public virtual async Task<List<TransactionHistory>> GetListAsync(
            string transactionNo = null,
            string customerId = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string sorting = null,
            CancellationToken cancellationToken = default)
        {
            return await (await GetDbSetAsync())
                .WhereIf(
                    !transactionNo.IsNullOrWhiteSpace(),
                    u =>
                        (u.TransactionNo != null && u.TransactionNo.Contains(transactionNo))
                )
                .WhereIf(
                    !customerId.IsNullOrWhiteSpace(),
                    u =>
                        (u.CustomerId.ToString() == customerId)
                )
                .OrderBy(sorting.IsNullOrWhiteSpace() ? nameof(TransactionHistory.TransactionNo) : sorting)
                .PageBy(skipCount, maxResultCount)
                .ToListAsync(GetCancellationToken(cancellationToken));
        }

        public virtual async Task<long> GetCountAsync(
            string transactionNo = null,
            string customerId = null,
            CancellationToken cancellationToken = default)
        {
            return await (await GetDbSetAsync())
                .WhereIf(
                    !transactionNo.IsNullOrWhiteSpace(),
                    u =>
                        (u.TransactionNo != null && u.TransactionNo.Contains(transactionNo))
                )
                .WhereIf(
                    !customerId.IsNullOrWhiteSpace(),
                    u =>
                        (u.CustomerId.ToString() == customerId)
                )
                .LongCountAsync(GetCancellationToken(cancellationToken));
        }
    }
}
