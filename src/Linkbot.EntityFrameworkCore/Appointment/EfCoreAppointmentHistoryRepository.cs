﻿using Linkbot.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Linkbot.Appointment
{
    public class EfCoreAppointmentHistoryRepository
        : EfCoreRepository<LinkbotDbContext, AppointmentHistory, Guid>,
            IAppointmentHistoryRepository
    {
        public EfCoreAppointmentHistoryRepository(
            IDbContextProvider<LinkbotDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public virtual async Task<List<AppointmentHistory>> GetListAsync(
            string appointmentId = null,
            string customerId = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string sorting = null,
            CancellationToken cancellationToken = default)
        {
            return await (await GetDbSetAsync())
                .WhereIf(
                    !appointmentId.IsNullOrWhiteSpace(),
                    u =>
                        (u.AppointmentId != null && u.AppointmentId.Contains(appointmentId))
                )
                .WhereIf(
                    !customerId.IsNullOrWhiteSpace(),
                    u =>
                        (u.CustomerId.ToString() == customerId)
                )
                .OrderBy(sorting.IsNullOrWhiteSpace() ? nameof(AppointmentHistory.AppointmentId) : sorting)
                .PageBy(skipCount, maxResultCount)
                .ToListAsync(GetCancellationToken(cancellationToken));
        }

        public virtual async Task<long> GetCountAsync(
            string appointmentId = null,
            string customerId = null,
            CancellationToken cancellationToken = default)
        {
            return await (await GetDbSetAsync())
                .WhereIf(
                    !appointmentId.IsNullOrWhiteSpace(),
                    u =>
                        (u.AppointmentId != null && u.AppointmentId.Contains(appointmentId))
                )
                .WhereIf(
                    !customerId.IsNullOrWhiteSpace(),
                    u =>
                        (u.CustomerId.ToString() == customerId)
                )
                .LongCountAsync(GetCancellationToken(cancellationToken));
        }
    }
}
