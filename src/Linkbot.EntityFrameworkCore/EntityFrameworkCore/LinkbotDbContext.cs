﻿using Microsoft.EntityFrameworkCore;
using Linkbot.Users;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.Identity;
using Volo.Abp.Users.EntityFrameworkCore;
using Linkbot.Customers;
using Linkbot.Transaction;
using Linkbot.Appointment;

namespace Linkbot.EntityFrameworkCore
{
    /* This is your actual DbContext used on runtime.
     * It includes only your entities.
     * It does not include entities of the used modules, because each module has already
     * its own DbContext class. If you want to share some database tables with the used modules,
     * just create a structure like done for AppUser.
     *
     * Don't use this DbContext for database migrations since it does not contain tables of the
     * used modules (as explained above). See LinkbotMigrationsDbContext for migrations.
     */
    [ConnectionStringName("Default")]
    public class LinkbotDbContext : AbpDbContext<LinkbotDbContext>
    {
        public DbSet<AppUser> Users { get; set; }

        public DbSet<CustomerDetails> CustomerDetails { get; set; }

        public DbSet<CustomerSettings> CustomerSettings { get; set; }

        public DbSet<CustomerCustomProperty> CustomerCustomProperty { get; set; }

        public DbSet<TransactionHistory> TransactionHistories { get; set; }

        public DbSet<AppointmentHistory> AppointmentHistories { get; set; }

        /* Add DbSet properties for your Aggregate Roots / Entities here.
         * Also map them inside LinkbotDbContextModelCreatingExtensions.ConfigureLinkbot
         */

        public LinkbotDbContext(DbContextOptions<LinkbotDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            /* Configure the shared tables (with included modules) here */

            builder.Entity<AppUser>(b =>
            {
                b.ToTable(AbpIdentityDbProperties.DbTablePrefix + "Users"); //Sharing the same table "AbpUsers" with the IdentityUser
                
                b.ConfigureByConvention();
                b.ConfigureAbpUser();

                /* Configure mappings for your additional properties
                 * Also see the LinkbotEfCoreEntityExtensionMappings class
                 */
            });

            /* Configure your own tables/entities inside the ConfigureLinkbot method */

            builder.ConfigureLinkbot();
        }
    }
}
