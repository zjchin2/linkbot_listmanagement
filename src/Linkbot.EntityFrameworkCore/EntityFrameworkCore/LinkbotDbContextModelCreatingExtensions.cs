﻿using Linkbot.Appointment;
using Linkbot.Customers;
using Linkbot.Transaction;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Linkbot.EntityFrameworkCore
{
    public static class LinkbotDbContextModelCreatingExtensions
    {
        public static void ConfigureLinkbot(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(LinkbotConsts.DbTablePrefix + "YourEntities", LinkbotConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});

            builder.Entity<CustomerDetails>(b =>
            {
                b.ToTable(LinkbotConsts.DbTablePrefix + "CustomerDetails", LinkbotConsts.DbSchema);
                b.ConfigureByConvention(); //auto configure for the base class props
            });

            builder.Entity<CustomerSettings>(b =>
            {
                b.ToTable(LinkbotConsts.DbTablePrefix + "CustomerSettings", LinkbotConsts.DbSchema);
                b.ConfigureByConvention(); //auto configure for the base class props
            });

            builder.Entity<CustomerCustomProperty>(b =>
            {
                b.ToTable(LinkbotConsts.DbTablePrefix + "CustomerCustomProperty", LinkbotConsts.DbSchema);
                b.ConfigureByConvention(); //auto configure for the base class props
            });

            builder.Entity<TransactionHistory>(b =>
            {
                b.ToTable(LinkbotConsts.DbTablePrefix + "TransactionHistory", LinkbotConsts.DbSchema);
                b.ConfigureByConvention(); //auto configure for the base class props
            });

            builder.Entity<AppointmentHistory>(b =>
            {
                b.ToTable(LinkbotConsts.DbTablePrefix + "AppointmentHistory", LinkbotConsts.DbSchema);
                b.ConfigureByConvention(); //auto configure for the base class props
            });
        }
    }
}