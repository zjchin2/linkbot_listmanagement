﻿using Linkbot.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Linkbot.Customers
{
    public class EfCoreCustomerSettingsRepository
        : EfCoreRepository<LinkbotDbContext, CustomerSettings, Guid>,
            ICustomerSettingsRepository
    {
        public EfCoreCustomerSettingsRepository(
            IDbContextProvider<LinkbotDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
