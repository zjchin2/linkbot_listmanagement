﻿using Linkbot.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Linkbot.Customers
{
    public class EfCoreCustomerCustomPropertyRepository
        : EfCoreRepository<LinkbotDbContext, CustomerCustomProperty, Guid>,
            ICustomerCustomPropertyRepository
    {
        public EfCoreCustomerCustomPropertyRepository(
            IDbContextProvider<LinkbotDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
