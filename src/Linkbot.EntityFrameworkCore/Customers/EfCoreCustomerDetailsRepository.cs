﻿using Linkbot.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Linkbot.Customers
{
    public class EfCoreCustomerDetailsRepository
        : EfCoreRepository<LinkbotDbContext, CustomerDetails, Guid>,
            ICustomerDetailsRepository
    {
        public EfCoreCustomerDetailsRepository(
            IDbContextProvider<LinkbotDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public virtual async Task<List<CustomerDetails>> GetListAsync(
            string filter = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string sorting = null,
            CancellationToken cancellationToken = default)
        {
            return await (await GetDbSetAsync())
                .WhereIf(
                    !filter.IsNullOrWhiteSpace(),
                    u =>
                        (u.FirstName != null && u.FirstName.ToLower().Contains(filter.ToLower()) ||
                        (u.EmailAddress != null && u.EmailAddress.ToLower().Contains(filter.ToLower())) ||
                        (u.MobileNumber != null && u.MobileNumber.ToLower().Contains(filter.ToLower())))
                )
                .OrderBy(sorting.IsNullOrWhiteSpace() ? nameof(CustomerDetails.FirstName) : sorting)
                .PageBy(skipCount, maxResultCount)
                .ToListAsync(GetCancellationToken(cancellationToken));
        }

        public virtual async Task<long> GetCountAsync(
            string filter = null,
            CancellationToken cancellationToken = default)
        {
            return await (await GetDbSetAsync())
                .WhereIf(
                    !filter.IsNullOrWhiteSpace(),
                    u =>
                        (u.FirstName != null && u.FirstName.Contains(filter) ||
                        (u.EmailAddress != null && u.EmailAddress.Contains(filter)) ||
                        (u.MobileNumber != null && u.MobileNumber.Contains(filter)))
                )
                .LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public virtual async Task UpdateExtraPropertyAsync(
            string inputValueBefore,
            string inputValueAfter,
            CancellationToken cancellationToken = default)
        {
            var dbContext = await GetDbContextAsync();

            var extraPropertiesList = from customer in dbContext.Set<CustomerDetails>()
                                      select new { customer.ExtraProperties, customer.Id };

            var toUpdate = extraPropertiesList.ToList()
                           .Select(c => new { Dic = c.ExtraProperties, c.Id })
                           .Where(c => c.Dic.Keys.Any(key => key == inputValueBefore))
                           .ToList();

            foreach (var ep in toUpdate)
            {
                foreach(var dic in ep.Dic)
                {
                    if(dic.Key == inputValueBefore)
                    {
                        object value = "";
                        value = dic.Value;
                        ep.Dic.Remove(inputValueBefore);
                        ep.Dic.Add(inputValueAfter, value);

                        var customer = dbContext.Set<CustomerDetails>().FirstOrDefault(x => x.Id == ep.Id);
                        customer.ExtraProperties = ep.Dic;
                        break;
                    }
                }
            }

            await dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
