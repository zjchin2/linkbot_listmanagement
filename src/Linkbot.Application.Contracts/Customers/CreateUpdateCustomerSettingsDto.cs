﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Linkbot.Validation;

namespace Linkbot.Customers
{
    public class CreateUpdateCustomerSettingsDto
    {
        [Display(Name = "Is Email Address mandatory?")]
        public bool IsEmailMandatory { get; set; } = true;

        [Display(Name = "Is Mobile Number mandatory?")]
        public bool IsMobileNumberMandatory { get; set; } = false;
    }
}
