﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Linkbot.Customers
{
    public interface ICustomerDetailsAppService :
        ICrudAppService<
            CustomerDetailsDto,
            Guid,
            PageLookupInputDto,
            CreateUpdateCustomerDetailsDto>,
        IApplicationService
    {
        Task<CustomerDetailsDto> CreateAsync(CreateUpdateCustomerDetailsDto input);

        Task<CustomerDetailsDto> UpdateAsync(Guid id, CreateUpdateCustomerDetailsDto input);

        Task<PagedResultDto<CustomerDetailsDto>> GetListAsync(PageLookupInputDto input);

        Task<List<CustomerDetailsDto>> GetListExportAsync(PageLookupInputDto input);
    }
}
