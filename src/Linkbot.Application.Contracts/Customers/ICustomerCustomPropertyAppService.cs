﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Linkbot.Customers
{
    public interface ICustomerCustomPropertyAppService :
        ICrudAppService<
            CustomerCustomPropertyDto,
            Guid,
            PagedAndSortedResultRequestDto,
            CustomerCustomPropertyDto>,
        IApplicationService
    {
        Task<PagedResultDto<CreateUpdateCustomPropertyDetailsDto>> GetListPageAsync();

        Task<List<CreateUpdateCustomPropertyDetailsDto>> GetCustomerCustomPropertyAsync();

        Task UpdateCustomPropertyAsync(Guid id, CustomerCustomPropertyDto input);
    }
}
