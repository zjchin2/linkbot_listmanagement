﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Customers
{
    public class CustomerCustomPropertyDto : AuditedEntityDto<Guid>
    {
        public string CustomPropertyType { get; set; }
        public string CustomPropertyName { get; set; }
        public bool IsHostProperty { get; set; } = false;
    }
}
