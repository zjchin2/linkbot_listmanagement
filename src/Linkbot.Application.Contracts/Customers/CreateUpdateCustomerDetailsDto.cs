﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Linkbot.Validation;
using Volo.Abp.Data;
using Volo.Abp.ObjectExtending;

namespace Linkbot.Customers
{
    public class CreateUpdateCustomerDetailsDto: ExtensibleObject
    {
        [StringLength(128)]
        public string FirstName { get; set; }

        [StringLength(128)]
        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        [RegularExpression(@"\d{8}$", ErrorMessage = "Invalid contact number.")]
        public string MobileNumber { get; set; }


        [EmailAddress]
        public string EmailAddress { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        [Range(0, Int64.MaxValue, ErrorMessage = "Invalid postal code.")]
        [StringLength(6)]
        public string PostalCode { get; set; }
        public string CompanyName { get; set; }
        public string CustomerStatus { get; set; }
        public string Subscription { get; set; }
        public string Remark { get; set; }
        public bool IsEmailMandatory { get; set; } = true;
        public bool IsMobileNumberMandatory { get; set; } = false;
    }
}
