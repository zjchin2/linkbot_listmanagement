﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Linkbot.Validation;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Data;
using Volo.Abp.ObjectExtending;

namespace Linkbot.Customers
{
    public class CreateUpdateCustomPropertyDetailsDto: EntityDto<Guid>
    {
        [Required]
        [StringLength(128)]
        [Display(Name = "Custom Property Name")]
        public string CustomPropertyName { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Custom Property Type")]
        public string CustomPropertyType { get; set; }
    }
}
