﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Linkbot.Customers
{
    public interface ICustomerSettingsAppService :
        ICrudAppService<
            CustomerSettingsDto,
            Guid,
            PagedAndSortedResultRequestDto,
            CreateUpdateCustomerSettingsDto>,
        IApplicationService
    {
        Task<CustomerSettingsDto> GetCustomerSettingsAsync();

        Task<CustomerSettingsDto> UpdateCustomerSettingsAsync(CreateUpdateCustomerSettingsDto input);
    }
}
