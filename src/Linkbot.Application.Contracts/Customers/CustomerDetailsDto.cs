﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Data;
using Volo.Abp.ObjectExtending;

namespace Linkbot.Customers
{
    public class CustomerDetailsDto : AuditedEntityDto<Guid>, IHasExtraProperties
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public string CompanyName { get; set; }
        public string CustomerStatus { get; set; }
        public string Subscription { get; set; }
        public string Remark { get; set; }
        public ExtraPropertyDictionary ExtraProperties { get; set; }
    }
}
