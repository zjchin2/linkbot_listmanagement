﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Customers
{
    public class CustomerSettingsDto : AuditedEntityDto<Guid>
    {
        public bool IsEmailMandatory { get; set; } = true;

        public bool IsMobileNumberMandatory { get; set; } = false;
    }
}
