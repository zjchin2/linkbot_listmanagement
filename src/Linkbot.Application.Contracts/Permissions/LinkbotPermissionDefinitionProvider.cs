﻿using Linkbot.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Linkbot.Permissions
{
    public class LinkbotPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var linkbotGroup = context.AddGroup(LinkbotPermissions.GroupName, L("Permission:CustomerManagement"));

            //Define your own permissions here. Example:
            //myGroup.AddPermission(LinkbotPermissions.MyPermission1, L("Permission:MyPermission1"));

            var customersPermission = linkbotGroup.AddPermission(LinkbotPermissions.Customers.Default, L("Permission:Customers"));
            customersPermission.AddChild(LinkbotPermissions.Customers.Create, L("Permission:Customers.Create"));
            customersPermission.AddChild(LinkbotPermissions.Customers.Edit, L("Permission:Customers.Edit"));
            customersPermission.AddChild(LinkbotPermissions.Customers.Delete, L("Permission:Customers.Delete"));


            var customerSettingsPermission = linkbotGroup.AddPermission(LinkbotPermissions.CustomerSettings.Default, L("Permission:CustomerSettings"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.MandatoryFieldListing, L("Permission:CustomerMandatoryFieldSettings.Listing"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.MandatoryFieldEdit, L("Permission:CustomerMandatoryFieldSettings.Edit"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.CustomPropertyListing, L("Permission:CustomersCustomProperty.Listing"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.CustomPropertyCreate, L("Permission:CustomersCustomProperty.Create"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.CustomPropertyEdit, L("Permission:CustomersCustomProperty.Edit"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.CustomPropertyDelete, L("Permission:CustomersCustomProperty.Delete"));
            customerSettingsPermission.AddChild(LinkbotPermissions.CustomerSettings.CustomPropertyHost, L("Permission:CustomersCustomProperty.Host"));

            var transactionHistoryPermission = linkbotGroup.AddPermission(LinkbotPermissions.TransactionHistory.Default, L("Permission:TransactionHistory"));
            transactionHistoryPermission.AddChild(LinkbotPermissions.TransactionHistory.Create, L("Permission:TransactionHistory.Create"));
            transactionHistoryPermission.AddChild(LinkbotPermissions.TransactionHistory.Edit, L("Permission:TransactionHistory.Edit"));
            transactionHistoryPermission.AddChild(LinkbotPermissions.TransactionHistory.Delete, L("Permission:TransactionHistory.Delete"));

            var appointmentHistoryPermission = linkbotGroup.AddPermission(LinkbotPermissions.AppointmentHistory.Default, L("Permission:AppointmentHistory"));
            appointmentHistoryPermission.AddChild(LinkbotPermissions.AppointmentHistory.Create, L("Permission:AppointmentHistory.Create"));
            appointmentHistoryPermission.AddChild(LinkbotPermissions.AppointmentHistory.Edit, L("Permission:AppointmentHistory.Edit"));
            appointmentHistoryPermission.AddChild(LinkbotPermissions.AppointmentHistory.Delete, L("Permission:AppointmentHistory.Delete"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<LinkbotResource>(name);
        }
    }
}
