﻿namespace Linkbot.Permissions
{
    public static class LinkbotPermissions
    {
        public const string GroupName = "Linkbot";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";

        public static class Customers
        {
            public const string Default = GroupName + ".Customers";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }

        public static class CustomerSettings
        {
            public const string Default = GroupName + ".CustomerSettings";
            public const string MandatoryFieldListing = Default + ".MandatoryFieldListing";
            public const string MandatoryFieldEdit = Default + ".MandatoryFieldEdit";
            public const string CustomPropertyListing = Default + ".CustomPropertyListing";
            public const string CustomPropertyCreate = Default + ".CustomPropertyCreate";
            public const string CustomPropertyEdit = Default + ".CustomPropertyEdit";
            public const string CustomPropertyDelete = Default + ".CustomPropertyDelete";
            public const string CustomPropertyHost = Default + ".CustomPropertyHost";
        }

        public static class TransactionHistory
        {
            public const string Default = GroupName + ".Transaction";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }

        public static class AppointmentHistory
        {
            public const string Default = GroupName + ".Appointment";
            public const string Create = Default + ".Create";
            public const string Edit = Default + ".Edit";
            public const string Delete = Default + ".Delete";
        }
    }
}