﻿using Linkbot.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Linkbot.Validation
{
    public class RequiredIfAttribute : ValidationAttribute
    {
        RequiredAttribute _innerAttribute = new RequiredAttribute();
        public string _dependentProperty { get; set; }
        public object _targetValue { get; set; }
        public string _label { get; set; }

        public RequiredIfAttribute(string dependentProperty, object targetValue, string label)
        {
            this._dependentProperty = dependentProperty;
            this._targetValue = targetValue;
            this._label = label;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var field = validationContext.ObjectType.GetProperty(_dependentProperty);
            if (field != null)
            {
                var dependentValue = field.GetValue(validationContext.ObjectInstance, null);

                if(_targetValue != null)
                {
                    if ((dependentValue == null && _targetValue == null) || (dependentValue != null && dependentValue.Equals(_targetValue)))
                    {
                        if (!_innerAttribute.IsValid(value))
                        {
                            string name = validationContext.DisplayName;
                            return new ValidationResult(ErrorMessage = _label + " is required.");
                        }
                    }
                    return ValidationResult.Success;
                }
                else
                {
                    if (dependentValue.Equals(true))
                    {
                        if (!_innerAttribute.IsValid(value))
                        {
                            string name = validationContext.DisplayName;
                            return new ValidationResult(ErrorMessage = _label + " is required.");
                        }
                    }
                    return ValidationResult.Success;
                }
            }
            else
            {
                return new ValidationResult(FormatErrorMessage(_dependentProperty));
            }
        }
    }
}
