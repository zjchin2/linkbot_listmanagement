﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Transaction
{
    public class GetTransactionHistoryInput : PagedAndSortedResultRequestDto
    {
        public string TransactionNo { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}
