﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Transaction
{
    public class TransactionHistoryDto : AuditedEntityDto<Guid>
    {
        public string PurchaseItem { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public double Amount { get; set; }
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string TransactionNo { get; set; }
        public string Ref { get; set; }
        public string ItemCategory { get; set; }

        public string CustomerId { get; set; }
    }
}
