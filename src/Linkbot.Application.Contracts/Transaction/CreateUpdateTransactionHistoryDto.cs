﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Transaction
{
    public class CreateUpdateTransactionHistoryDto
    {
        public string PurchaseItem { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public double Amount { get; set; }
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string TransactionNo { get; set; }
        public string Ref { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ItemCategory { get; set; }
    }
}
