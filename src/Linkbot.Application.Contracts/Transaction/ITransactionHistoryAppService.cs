﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Linkbot.Transaction
{
    public interface ITransactionHistoryAppService :
        ICrudAppService<
            TransactionHistoryDto,
            Guid,
            GetTransactionHistoryInput,
            CreateUpdateTransactionHistoryDto>,
        IApplicationService
    {
        Task<TransactionHistoryDto> CreateAsync(CreateUpdateTransactionHistoryDto input);
        Task<PagedResultDto<CreateUpdateTransactionHistoryDto>> GetPurchaseHistoryAsync(PageLookupInputDto input);
    }
}
