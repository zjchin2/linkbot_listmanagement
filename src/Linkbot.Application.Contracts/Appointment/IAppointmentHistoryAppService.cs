﻿using Linkbot.Appointment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Linkbot.Appointment
{
    public interface IAppointmentHistoryAppService :
        ICrudAppService<
            AppointmentHistoryDto,
            Guid,
            GetAppointmentHistoryInput,
            CreateUpdateAppointmentHistoryDto>,
        IApplicationService
    {
        Task<AppointmentHistoryDto> CreateAsync(CreateUpdateAppointmentHistoryDto input);

        Task<PagedResultDto<CreateUpdateAppointmentHistoryDto>> GetUpcomingApptListPageAsync(PageLookupInputDto input);
    }
}
