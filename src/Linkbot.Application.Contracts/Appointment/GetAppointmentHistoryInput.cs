﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Appointment
{
    public class GetAppointmentHistoryInput : PagedAndSortedResultRequestDto
    {
        public string AppointmentId { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}
