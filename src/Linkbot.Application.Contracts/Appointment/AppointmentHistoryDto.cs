﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Linkbot.Appointment
{
    public class AppointmentHistoryDto : AuditedEntityDto<Guid>
    {
        public string AppointmentName { get; set; }
        public string AppointmentStatus { get; set; }
        public string AppointmentId { get; set; }
        public DateTimeOffset AppointmentDate { get; set; }
        public DateTimeOffset BookingDate { get; set; }
        public DateTimeOffset CancellationDate { get; set; }
    }
}
