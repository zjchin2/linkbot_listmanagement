﻿using Linkbot.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace Linkbot.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(LinkbotEntityFrameworkCoreDbMigrationsModule),
        typeof(LinkbotApplicationContractsModule)
        )]
    public class LinkbotDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
