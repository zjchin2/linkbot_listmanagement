﻿using Volo.Abp.Modularity;

namespace Linkbot
{
    [DependsOn(
        typeof(LinkbotApplicationModule),
        typeof(LinkbotDomainTestModule)
        )]
    public class LinkbotApplicationTestModule : AbpModule
    {

    }
}