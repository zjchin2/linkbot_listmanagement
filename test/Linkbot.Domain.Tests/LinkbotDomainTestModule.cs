﻿using Linkbot.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Linkbot
{
    [DependsOn(
        typeof(LinkbotEntityFrameworkCoreTestModule)
        )]
    public class LinkbotDomainTestModule : AbpModule
    {

    }
}